using Enterprises.Application.Interfaces;
using Enterprises.Domain.Core.Notifications;
using Enterprises.Domain.Interfaces;
using Enterprises.Services.Api.Controllers;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Enterprises.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v1/[controller]")]
    public class EnterprisesController : ApiController
    {
        private readonly IEnterpriseAppService enterpriseAppService;

        public EnterprisesController(IEnterpriseAppService enterpriseAppService,
            INotificationHandler<DomainNotification> notifications,
            IMediatorHandler mediator)
            : base(notifications, mediator)
        {
            this.enterpriseAppService = enterpriseAppService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Response(await enterpriseAppService.GetAll());
        }

        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> GetEnterpriseById(int id)
        {
            return Response(await enterpriseAppService.GetById(id));

        }

        [HttpGet("ShowEnterpriseByNameAndType")]
        public async Task<IActionResult> GetEnterpriseFilter(string name, int typeId )
        {
            return Response(await enterpriseAppService.GetEnterpriseFilter( name, typeId));
        }
    }

}
