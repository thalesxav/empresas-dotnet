﻿using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace Enterprises.Services.Api.Configuration
{
    public static class SwaggerConfiguration
    {
        public static void AddSwaggerConfig(this IServiceCollection services)
        {
            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "IOASYS",
                    Description = "Teste Web Api",
                    TermsOfService = "",
                    Contact = new Contact { Name = "Thales Xavier", Email = "xavier.thales@gmail.com", Url = "https://www.linkedin.com/in/thales-xavier-almeida-nogueira/" },
                    License = new License { Name = "", Url = "" }
                });
            });
        }
    }
}
