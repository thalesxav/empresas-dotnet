﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Enterprises.Domain.Interfaces;

namespace Enterprises.Domain.Enterprises.Repository
{
    public interface IEnterpriseRepository : IRepository<Enterprise>
    {
        Task<List<Enterprise>> GetEnterpriseFilterAsync(string name, int typeId);
    }
}
