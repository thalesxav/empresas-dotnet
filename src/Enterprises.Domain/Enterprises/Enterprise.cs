﻿using FluentValidation;
using Enterprises.Domain.Core.Models;

namespace Enterprises.Domain.Enterprises
{
    public class Enterprise : Entity<Enterprise>
    {

        #region constructors
        protected Enterprise() { }
        #endregion

        #region Properties
        public string Enterprise_Name { get; private set; }
        public string Description { get; private set; }
        public string Email_Enterprise { get; private set; }
        public string Facebook { get; private set; }
        public string Twitter { get; private set; }
        public string Linkedin { get; private set; }
        public string Phone { get; private set; }
        public bool Own_Enterprise { get; private set; }
        public string Photo { get; private set; }
        public int Value { get; private set; }
        public int Shares { get; private set; }
        public int Share_Price { get; private set; }
        public int Own_Shares { get; private set; }
        public string City { get; private set; }
        public string Country { get; private set; }
        public int TypeId { get; private set; }
        public virtual  EnterpriseType EnterpriseType { get; private set; }
        #endregion

        #region Methods
        public override bool IsValid()
        {
            Validate();
            return ValidationResult.IsValid;
        }

        public override void Validate()
        {
            ValidateName();
            ValidateDescription();
        }

        private void ValidateName()
        {
            RuleFor(c => c.Enterprise_Name)
                .NotEmpty().WithMessage("O nome da Empresa precisa ser fornecido")
                .Length(2, 200).WithMessage("O nome da Empresa precisa ter entre 2 e 150 caracteres");
        }

        private void ValidateDescription()
        {
            RuleFor(c => c.Description)
                .NotEmpty().WithMessage("A Descrição da Empresa precisa ser fornecida")
                .Length(2, 500).WithMessage("A Descrição do Empresa precisa ter entre 2 e 500 caracteres");
        }
        #endregion

    }
}
