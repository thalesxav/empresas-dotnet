﻿using FluentValidation;
using Enterprises.Domain.Core.Models;
using System.Collections.Generic;

namespace Enterprises.Domain.Enterprises
{
    public class EnterpriseType : Entity<EnterpriseType>
    {

        #region constructors
        public EnterpriseType(){}
        #endregion

        public string EnterpriseTypeName { get; set; }

        // EF Propriedade de Navegação
        public virtual ICollection<Enterprise> Enterprises { get; set; }

        #region Methods
        public override bool IsValid()
        {
            Validate();
            return ValidationResult.IsValid;
        }

        public override void Validate()
        {
            ValidateName();
        }

        private void ValidateName()
        {
            RuleFor(c => c.EnterpriseTypeName)
                .NotEmpty().WithMessage("O nome do tipo da Empresa precisa ser fornecido")
                .Length(2, 200).WithMessage("O nome do tipo da  Empresa precisa ter entre 2 e 150 caracteres");
        }

        
        #endregion
    }
}
