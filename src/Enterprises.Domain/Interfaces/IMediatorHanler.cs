﻿using Enterprises.Domain.Core.Commands;
using Enterprises.Domain.Core.Events;
using System.Threading.Tasks;

namespace Enterprises.Domain.Interfaces
{
    public interface IMediatorHandler
    {
        Task SendCommand<T>(T Command) where T : Command;

        Task RaiseEvent<T>(T Event) where T : Event;
    }
}
