﻿using System;
using System.Threading.Tasks;

namespace Enterprises.Domain.Interfaces
{
    public interface IUnityOfWork : IDisposable
    {
        Task<bool> Commit();
    }
}
