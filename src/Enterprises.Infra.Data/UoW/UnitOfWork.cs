﻿using System.Threading.Tasks;
using Enterprises.Infra.Data.Context;
using Enterprises.Domain.Interfaces;

namespace Enterprises.Infra.Data.UoW
{
    public class UnitOfWork : IUnityOfWork
    {
        private readonly EnterprisesContext _context;

        public UnitOfWork(EnterprisesContext context)
        {
            _context = context;
        }

        public async Task<bool> Commit()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Dispose()
        {
            _context.Dispose();
        }

       
    }
}
