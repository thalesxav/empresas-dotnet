﻿using Enterprises.Infra.Data.Extensions;
using Enterprises.Infra.Data.Mappings;
using Enterprises.Domain.Enterprises;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Enterprises.Infra.Data.Context
{
    public class EnterprisesContext : IdentityDbContext
    {
        public DbSet<Enterprise> Enterprises { get; set; }
        
        public EnterprisesContext (DbContextOptions<EnterprisesContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.AddConfiguration(new EnterpriseMapping());
            modelBuilder.AddConfiguration(new EnterpriseTypeMapping());
            //modelBuilder.Seed();
            
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            optionsBuilder.UseSqlServer(config.GetConnectionString("DefaultConnection"));
        }
    }
}
