﻿using Enterprises.Infra.Data.Context;
using Enterprises.Domain.Enterprises;
using Enterprises.Domain.Enterprises.Repository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enterprises.Infra.Data.Repositorys
{
    public class EnterpriseRepository : Repository<Enterprise>, IEnterpriseRepository
    {
        public EnterpriseRepository(EnterprisesContext context) : base(context)
        {
        }

        public async Task<List<Enterprise>> GetEnterpriseFilterAsync(string name, int typeId)
        {
            return await Db.Enterprises.Include(t => t.EnterpriseType).Where(p => p.Enterprise_Name == name && p.TypeId == typeId).ToListAsync();
        }
    }
}
