using Enterprises.Infra.Data.Extensions;
using Enterprises.Domain.Enterprises;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Enterprises.Infra.Data.Mappings
{    
    class EnterpriseMapping : EntityTypeConfiguration<Enterprise>
    {
        public override void Map(EntityTypeBuilder<Enterprise> builder)
        {
            builder.ToTable("Enterprises");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Enterprise_Name)
                .HasColumnType("varchar(200)")
                .IsRequired();
            builder.Property(e => e.Description)
                .HasColumnType("varchar(500)")
                .IsRequired();


            builder.HasOne(e => e.EnterpriseType)
              .WithMany(o => o.Enterprises)
              .HasForeignKey(e => e.TypeId);

            builder.Ignore(e => e.ValidationResult);
            builder.Ignore(e => e.CascadeMode);
        }
    }
}
