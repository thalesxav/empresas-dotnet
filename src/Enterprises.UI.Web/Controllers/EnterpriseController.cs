using System;
using Enterprises.Application.Interfaces;
using Enterprises.Application.ViewModels;
using Enterprises.Domain.Core.Notifications;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Enterprises.UI.Web.Controllers
{
    [Authorize]
    public class EnterpriseController : BaseController
    {
        private readonly IEnterpriseAppService _EnterpriseAppService;

        public EnterpriseController(IEnterpriseAppService EnterpriseAppService, 
                                  INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
            _EnterpriseAppService = EnterpriseAppService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("Enterprise-management/list-all")]
        public IActionResult Index()
        {
            return View(_EnterpriseAppService.GetAll());
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("Enterprise-management/Enterprise-details/{id:guid}")]
        public IActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var EnterpriseViewModel = _EnterpriseAppService.GetById(id.Value);

            if (EnterpriseViewModel == null)
            {
                return NotFound();
            }

            return View(EnterpriseViewModel);
        }

        [HttpGet]
        [Authorize(Roles = "CanWriteEnterpriseData")]
        [Route("Enterprise-management/register-new")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "CanWriteEnterpriseData")]
        [Route("Enterprise-management/register-new")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(EnterpriseViewModel EnterpriseViewModel)
        {
            if (!ModelState.IsValid) return View(EnterpriseViewModel);
            _EnterpriseAppService.Register(EnterpriseViewModel);

            if (IsValidOperation())
                ViewBag.Sucesso = "Enterprise Registered!";

            return View(EnterpriseViewModel);
        }
        
        [HttpGet]
        [Authorize(Roles = "CanWriteEnterpriseData")]
        [Route("Enterprise-management/edit-Enterprise/{id:guid}")]
        public IActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var EnterpriseViewModel = _EnterpriseAppService.GetById(id.Value);

            if (EnterpriseViewModel == null)
            {
                return NotFound();
            }

            return View(EnterpriseViewModel);
        }

        [HttpPost]
        [Authorize(Roles = "CanWriteEnterpriseData")]
        [Route("Enterprise-management/edit-Enterprise/{id:guid}")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EnterpriseViewModel EnterpriseViewModel)
        {
            if (!ModelState.IsValid) return View(EnterpriseViewModel);

            _EnterpriseAppService.Update(EnterpriseViewModel);

            if (IsValidOperation())
                ViewBag.Sucesso = "Enterprise Updated!";

            return View(EnterpriseViewModel);
        }

        [HttpGet]
        [Authorize(Roles = "CanRemoveEnterpriseData")]
        [Route("Enterprise-management/remove-Enterprise/{id:guid}")]
        public IActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var EnterpriseViewModel = _EnterpriseAppService.GetById(id.Value);

            if (EnterpriseViewModel == null)
            {
                return NotFound();
            }

            return View(EnterpriseViewModel);
        }

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "CanRemoveEnterpriseData")]
        [Route("Enterprise-management/remove-Enterprise/{id:guid}")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(Guid id)
        {
            _EnterpriseAppService.Remove(id);

            if (!IsValidOperation()) return View(_EnterpriseAppService.GetById(id));

            ViewBag.Sucesso = "Enterprise Removed!";
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        [Route("Enterprise-management/Enterprise-history/{id:guid}")]
        public JsonResult History(Guid id)
        {
            var EnterpriseHistoryData = _EnterpriseAppService.GetAllHistory(id);
            return Json(EnterpriseHistoryData);
        }
    }
}
