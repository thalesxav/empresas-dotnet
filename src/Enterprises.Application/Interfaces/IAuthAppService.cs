﻿using Enterprises.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Enterprises.Application.Interfaces
{
    public interface IAuthAppService : IDisposable
    {
        Task<object> Authorize(string user, string pass);
    }
}
