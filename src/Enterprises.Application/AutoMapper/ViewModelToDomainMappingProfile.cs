﻿using AutoMapper;
using Enterprises.Application.ViewModels;
using Enterprises.Domain.Enterprises;

namespace Enterprises.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<EnterpriseViewModel, Enterprise>();
            CreateMap<Enterprise,EnterpriseViewModel>();

        }
    }
}
