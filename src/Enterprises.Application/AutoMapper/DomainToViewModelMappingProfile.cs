﻿using AutoMapper;
using Enterprises.Application.ViewModels;
using Enterprises.Domain.Enterprises;

namespace Enterprises.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Enterprise, EnterpriseViewModel>();
            CreateMap<Enterprise, EnterpriseViewModel>();
        }
    }
}
