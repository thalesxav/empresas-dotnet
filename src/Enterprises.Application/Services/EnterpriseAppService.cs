﻿using AutoMapper;
using Enterprises.Application.Interfaces;
using Enterprises.Application.ViewModels;
using Enterprises.Domain.Enterprises.Repository;
using Enterprises.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Enterprises.Application.Services.Enterprise
{
    public class EnterpriseAppService : IEnterpriseAppService
    {
        private IEnterpriseRepository enterpriseRepository;
        private readonly IMapper mapper;
        private readonly IMediatorHandler mediator;

        public EnterpriseAppService(IEnterpriseRepository enterpriseRepository, IMapper mapper, IMediatorHandler mediator)
        {
            this.enterpriseRepository = enterpriseRepository;
            this.mapper = mapper;
            this.mediator = mediator;
        }

        public Task Add(EnterpriseViewModel entity)
        {
            throw new NotImplementedException();
        }

        public async Task<EnterpriseViewModel> GetById(int id)
        {
            return mapper.Map<EnterpriseViewModel>(await enterpriseRepository.GetById(id));
        }

        public async Task<List<EnterpriseViewModel>> GetAll()
        {
            return mapper.Map<List<EnterpriseViewModel>>(await enterpriseRepository.GetAll());
        }

        public async Task Remove(int id)
        {
            await enterpriseRepository.Remove(id);
        }

        public async Task<List<EnterpriseViewModel>> GetEnterpriseFilter(string name, int typeId)
        {
            return mapper.Map<List<EnterpriseViewModel>>(await enterpriseRepository.GetEnterpriseFilterAsync(name, typeId));
        }

        public void Dispose()
        {
            enterpriseRepository.Dispose();
        }
    }
}
