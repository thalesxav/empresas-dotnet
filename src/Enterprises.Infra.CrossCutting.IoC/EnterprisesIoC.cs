﻿using AutoMapper;
using Enterprises.Application.AutoMapper;
using Enterprises.Application.Interfaces;
using Enterprises.Application.Services.Enterprise;
using Enterprises.CrossCutting.Integrator;
using Enterprises.Infra.Data.Context;
using Enterprises.Infra.Data.Repositorys;
using Enterprises.Infra.Data.UoW;
using Enterprises.Domain.Core.Notifications;
using Enterprises.Domain.Enterprises.Repository;
using Enterprises.Domain.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Enterprises.CrossCutting.IoC
{
    public static class EnterprisesIoC
    {
        public static void RegisterServices(this IServiceCollection services)
        {

            #region Application
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IConfigurationProvider>(AutoMapperConfig.RegisterMappings());
            services.AddScoped<IMapper>(sp => new Mapper(sp.GetRequiredService<IConfigurationProvider>(), sp.GetService));
            services.AddScoped<IEnterpriseAppService, EnterpriseAppService>();
            #endregion

            #region Domain

            services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>();
            services.AddScoped<IMediatorHandler, MediatorHandler>();
            #endregion

            #region data
            services.AddScoped<IUnityOfWork, UnitOfWork>();
            services.AddScoped<EnterprisesContext>();
            #endregion

            #region Repositories
            services.AddScoped<IEnterpriseRepository, EnterpriseRepository>();
            #endregion

          
        }
    }
}
